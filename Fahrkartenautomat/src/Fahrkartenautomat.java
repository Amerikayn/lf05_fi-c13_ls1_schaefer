﻿import java.util.Scanner;

class Fahrkartenautomat
{
	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("1. Einzelfahrschein Berlin AB 2,90€");
		System.out.println("2. Einzelfahrschein Berlin BC 3,30€");
		System.out.println("3. Einzelfahrschein Berlin ABC 3,60€");
		System.out.println("4. Kurzstrecke 1,90€");
		System.out.println("5. Tageskarte Berlin AB 8,60€");
		System.out.println("6. Tageskarte Berlin BC 9,00€");
		System.out.println("7. Tageskarte Berlin ABC 9,60€");
		System.out.println("8. Kleingruppen-Tageskarte Berlin AB 23,50€");
		System.out.println("9. Kleingruppen-Tageskarte Berlin BC 24,30€");
		System.out.println("10. Kleingruppen-Tageskarte Berlin ABC 24,90€");
		System.out.println("Wählen sie ihr Ticket der Nummer entsprechend: ");
		
		int kartenPreis = tastatur.nextInt();
		double zuZahlenderBetrag; 
	    int anzahlTickets; 
	    double gesamtBetrag;		
		double[] fahrkartenPreis = new double[11];
		
		fahrkartenPreis[1] = 2.9;
		fahrkartenPreis[2] = 3.3;
		fahrkartenPreis[3] = 3.6;
		fahrkartenPreis[4] = 1.9;
		fahrkartenPreis[5] = 8.6;
		fahrkartenPreis[6] = 9.0;
		fahrkartenPreis[7] = 9.6;
		fahrkartenPreis[8] = 23.5;
		fahrkartenPreis[9] = 24.3;
		fahrkartenPreis[10] = 24.9;
	    
	    
	    System.out.print("Anzahl der Tickets: ");
		anzahlTickets = tastatur.nextByte();
		zuZahlenderBetrag = fahrkartenPreis[kartenPreis] * anzahlTickets;
		System.out.printf("Zu zahlender Betrag (EURO): %.2f Euro\n" , zuZahlenderBetrag);
		
		gesamtBetrag = zuZahlenderBetrag;
		return gesamtBetrag;
		
	}
	
	public static double fahrkartenBezahlen(double a) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double eingeworfeneMünze;
		double gesamtBetrag = a;
		
		eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < gesamtBetrag) //Hier wird verglichen ob eingezahlterGesamtbetrag kleiner als zuZahlenderBetrag ist
			{
	    	   System.out.printf("Noch zu zahlen: %.2f Euro\n" , (a - eingezahlterGesamtbetrag)); //Hier wird eingezahlterGesamtbetrag von zuZahlenderBetrag abgezogen  
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	    	   gesamtBetrag = a;
	    	   eingezahlterGesamtbetrag += eingeworfeneMünze;
			}
		double rückgabebetrag = eingezahlterGesamtbetrag - gesamtBetrag;
		return rückgabebetrag;
	}
	
	public static void farhkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	}
	
	public static double rueckgeldAusgeben(double a) {
		   
	       double rückgabebetrag = a;
	       
	       if(rückgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro\n  " , rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
	           {
	        	  System.out.println("5 CENT");
	 	          rückgabebetrag -= 0.05;
	           }
	       }
	       return a;
	}
		
	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rueckgabeBetrag;
		
		zuZahlenderBetrag = fahrkartenbestellungErfassen();
		rueckgabeBetrag = fahrkartenBezahlen(zuZahlenderBetrag);
		farhkartenAusgeben();
		rueckgeldAusgeben(rueckgabeBetrag);
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
		
		main(args);
	}
	
}	
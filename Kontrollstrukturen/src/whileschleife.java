import java.util.Scanner;

public class whileschleife {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.print("Geben sie ihre Zahl ein: ");
		int zahl = myScanner.nextInt();
		int n = 1;
		int ergebnis = 1;
		
		while(n <= zahl)
		{
			ergebnis = ergebnis*n;
			System.out.print(n + " * ");
			n++;
		}
		
		System.out.print(ergebnis);
	}

}

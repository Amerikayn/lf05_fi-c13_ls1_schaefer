import java.util.Scanner;

public class PCHaendler {

	
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		System.out.println("was m�chten Sie bestellen?");
		String artikel = liesString();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesDouble();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		//rechungausgeben(anzahl, preis, bruttogesamtpreis, mwst, nettogesamtpreis);
		

	}

	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		String text = myScanner.next();
		return text;
	
	}
	
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		int a = myScanner.nextInt();
		return a;
	}
		
	public static double liesDouble() {
		Scanner myScanner = new Scanner(System.in);
		double b = myScanner.nextInt();
		return b;
	}
	public static double berechneGesamtnettopreis(int anzahl, double
	nettopreis) {
		return anzahl*nettopreis;
	}
	public static double berechneGesamtbruttopreis(double nettogesamtpreis,
	double mwst) {
		return nettogesamtpreis*(1+mwst/100);
	}
	public static void rechungausgeben(String artikel, int anzahl, double
	nettogesamtpreis, double bruttogesamtpreis,
	double mwst) {
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}
	
}
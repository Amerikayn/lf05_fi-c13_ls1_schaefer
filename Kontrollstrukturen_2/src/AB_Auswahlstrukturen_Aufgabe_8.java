import java.util.Scanner;

public class AB_Auswahlstrukturen_Aufgabe_8 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		//Variablen
		int jahreszahl;
		boolean obSchaltjahr;
		
		System.out.println("Geben sie ihr wunsch Jahre ein: ");
		jahreszahl = eingabe.nextInt();
		
		obSchaltjahr = schaltjahr(jahreszahl);
		
		//Ausgabe
		if (obSchaltjahr == true) 
		{
			System.out.println("Das Jahr " + jahreszahl + " ist ein Schaltjahr");
		}
		
		if (obSchaltjahr == false)
		{
			System.out.println("Das Jahr " + jahreszahl + " ist kein Schaltjahr");
		}
		
	}
		
	//Schaltjahr Ermittlung
	public static boolean schaltjahr(int a) {
			  if (a % 4 != 0)  
			  { 
			    return false;
			  } 
			  else if (a % 400 == 0) 
			  {
			    return true;
			  } 
			  
			  else if (a % 100 == 0) 
			  {
			    return false;
			  } 
			  
			  else 
			  {
			    return true;
			  }
			

	}
}

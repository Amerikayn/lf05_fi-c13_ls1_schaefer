import java.util.Scanner;

public class AB_Strukturiertes_Programmieren_Aufgabe_4 {

	public static void main(String[] args) {
		 Scanner myScanner = new Scanner(System.in);

		 double zahl1;
		 char operator;
		 double zahl2;
		 double ergebnis = 0;
		 
		 System.out.print("Bitte geben Sie eine Zahl ein: ");
		 zahl1 = myScanner.nextInt();

		 System.out.print("Bitte geben sie den Rechenoperator ein: ");
		 operator = myScanner.next().charAt(0);
		 
		 System.out.print("Bitte geben Sie eine zweite Zahl ein: ");
		 zahl2 = myScanner.nextInt();
		 
		 if (operator == '+'){
			 ergebnis = zahl1 + zahl2;
		 }
		 else if (operator == '-'){
			 ergebnis = zahl1 - zahl2;
		 }
		 else if (operator == '*'){
			 ergebnis = zahl1 * zahl2;
		 }
		 else if (operator == '/'){
			 ergebnis = zahl1 / zahl2;
		 }
		 
		 switch(operator) {
		 case '+':
			 System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
			 break;
		 case '-':
			 System.out.println(zahl1 + " - " + zahl2 + " = " + ergebnis);
			 break;
		 case '*':
			 System.out.println(zahl1 + " * " + zahl2 + " = " + ergebnis);
			 break;
		 case '/':
			 System.out.println(zahl1 + " / " + zahl2 + " = " + ergebnis);
			 break;
		 }		 
	}
}
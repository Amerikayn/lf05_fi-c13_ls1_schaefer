import java.util.Scanner;

public class AB_Schleifen1_Aufgabe_8 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		int spalten;
		
		System.out.println("Bitte geben Sie ihre Wunsch gr��e ein:");
		spalten = eingabe.nextInt();

		
		for (int i = 1; i <= spalten; i++) 
		{
			for (int j = 1; j <= spalten; j++)
			{
				if (i == 1 || i == spalten || j == 1 || j == spalten )
				{
					System.out.print("* ");
				}
				else
				{
					System.out.print("  ");
				}
			}
			System.out.println();
		}


	}

}

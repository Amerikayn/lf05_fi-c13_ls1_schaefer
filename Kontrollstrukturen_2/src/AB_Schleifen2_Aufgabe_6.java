import java.util.Scanner;

public class AB_Schleifen2_Aufgabe_6 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		
		//Variablen
		double einlage;
		double zinssatz;
		double ergebnis= 0;
		int jahre = 0;
		
		//Eingabe der Variablen
		System.out.print("Bitte geben sie die H�he ihrer Einlage ein: ");
		einlage = eingabe.nextDouble();
		System.out.print("Bitte geben sie die H�he des Zinssatzes ein: ");
		zinssatz = eingabe.nextDouble();
		
		while (einlage <1000000)
		{
			einlage = einlage * zinssatz;
			jahre++;		
		}
		
		System.out.println("Du bist Million�r in: "+ jahre + " Jahren");
		System.out.println("M�chten sie noch eine Rechnung machen? (j/n)");
		char ansage = eingabe.next().charAt(0);
		
		if (ansage == 'j')
		{
			main(args);
		}
		else if (ansage == 'n')
		{
			System.exit(0);
		}
		
	}
}

import java.util.Scanner;

public class AB_Schleifen2_Aufgabe_8 {

	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);

		
		int wunschzahl;
		int counter = 1;
		
		System.out.print("Bitte geben sie ihr Wunschzahl ein (zwischen 2 und 9): ");
		wunschzahl = eingabe.nextInt();
		
		while (counter < 100)
		{
			int a = counter / 10;
			int b = counter % 10;
			
			if (counter % wunschzahl  == 0)
			{
				System.out.printf("%5s", "*");
			}
			else if (a == wunschzahl || b == wunschzahl)
			{
				System.out.printf("%5s", "*");
			}
			else if (a + b == wunschzahl)
			{
				System.out.printf("%5s", "*");
			}
			else
			{
				System.out.printf("%5s", counter);
			}
			
			counter++;
			if (counter % 10 == 0 && counter != 0)
			{
				System.out.println();
			}
		}
	}

}


public class Aufgabe3 {

	public static void main(String[] args) {
		
		int f1 = -20;
		int f2 = -10;
		int f3 = 0;
		int f4 = 20;
		int f5 = 30;
		double c1 = -28.8889;
		double c2 = -23.3333;
		double c3 = -17.7778;
		double c4 = -6.6667;
		double c5 = -1.1111;
		
		
		System.out.printf("Fahrenheit");
		System.out.printf("%2s %3s\n", "|" , "Celsius");
		System.out.println("----------------------");
		System.out.print(f1);
		System.out.printf("%9s", "|");
		System.out.printf("%9.2f\n", c1);
		System.out.print(f2);
		System.out.printf("%9s", "|");
		System.out.printf("%9.2f\n", c2);
		System.out.print(f3);
		System.out.printf("%11s", "|");
		System.out.printf("%9.2f\n", c3);
		System.out.print(f4);
		System.out.printf("%10s", "|");
		System.out.printf("%9.2f\n", c4);
		System.out.print(f5);
		System.out.printf("%10s", "|");
		System.out.printf("%9.2f\n", c5);
		

	}

}
